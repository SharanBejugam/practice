﻿using MediatR;
using Sharan_25_Nov_Code_1.Queries;
using Sharan_25_Nov_Code_1.Repository;

namespace Sharan_25_Nov_Code_1.Handlers
{
    public class GetAllDataHandler : IRequestHandler<GetAllDataQuery, IEnumerable<WeatherForecast>>
    {
        private readonly IServices ser;
        public GetAllDataHandler(IServices ser)
        {
           this.ser = ser;  
        }

        public async Task<IEnumerable<WeatherForecast>> Handle(GetAllDataQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(ser.GetForecast());
        }
    }
}
