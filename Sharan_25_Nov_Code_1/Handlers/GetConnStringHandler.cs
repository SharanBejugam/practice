﻿using MediatR;
using Sharan_25_Nov_Code_1.Queries;
using Sharan_25_Nov_Code_1.Repository;

namespace Sharan_25_Nov_Code_1.Handlers
{
    public class GetConnStringHandler : IRequestHandler<GetConnStringQuery, string>
    {
       private readonly IServices services;

        public GetConnStringHandler(IServices services)
        {
            this.services = services;
        }

        public async Task<string> Handle(GetConnStringQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(services.GetConnString());
        }
    }
}
