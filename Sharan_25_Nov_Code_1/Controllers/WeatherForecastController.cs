using MediatR;
using Microsoft.AspNetCore.Mvc;
using Sharan_25_Nov_Code_1.Queries;

namespace Sharan_25_Nov_Code_1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private IMediator _mediator;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,IMediator mediator)
        {
            
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            return await _mediator.Send(new GetAllDataQuery());
        }

        [HttpGet("GetConnString")]
        public async Task<string> GetConnString() {
            return await _mediator.Send(new GetConnStringQuery());
        }
    }
}