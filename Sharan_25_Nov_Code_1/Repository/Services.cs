﻿namespace Sharan_25_Nov_Code_1.Repository
{
    public class Services:IServices
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };
        private IConfiguration conf;
        public Services(IConfiguration conf)
        {
            this.conf = conf;
        }

        public string GetConnString()
        {
            var con =  conf.GetSection("ConnectionStrings");
            return $"{con["Source"]}\n{con["InitialCatalog"]}\n{con["IntegratedSecurity"]}\n{con["PersistSecurityInfo"]}";
        }

        public IEnumerable<WeatherForecast> GetForecast()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
           .ToArray();
        }
    }
}
