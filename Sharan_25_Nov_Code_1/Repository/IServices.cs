﻿namespace Sharan_25_Nov_Code_1.Repository
{
    public interface IServices
    {
        public string GetConnString();
        public IEnumerable<WeatherForecast> GetForecast();
    }
}
